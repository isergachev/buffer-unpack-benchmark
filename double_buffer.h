#ifndef DRAGON_ZMQ_C_DOUBLE_BUFFER_H
#define DRAGON_ZMQ_C_DOUBLE_BUFFER_H


using namespace std;

// The class allows one writer and many reader threads use 2 buffers.
// The writer gets every time another buffer, unless it is used by any reader.
// In this case the writer will get the same buffer again.

template<typename T>
class double_buffer
{
public:
    explicit double_buffer(size_t capacity):
    last_written(0),
    reader_count(0)
    {
        v[0].resize(capacity);
        v[1].resize(capacity);
        for (uint i = 0; i < capacity; i ++)
        {
                v[0][i] = 100;
                v[1][i] = 200;
        }
    }

    vector<T> *get_buffer_to_write()
    {
        return &v[1 - last_written];
    }

    void return_written_buffer()
    {
        if (!reader_count)
            last_written = 1 - last_written;
    }

    const vector<T> *get_buffer_to_read() const
    {
        reader_count ++;
        return &v[last_written];
    }

    void return_read_buffer()
    {
        reader_count --;
    }

private:
    vector<T> v[2];
    volatile mutable atomic_int last_written, reader_count;
};


#endif //DRAGON_ZMQ_C_DOUBLE_BUFFER_H
