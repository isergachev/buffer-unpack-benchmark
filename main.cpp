#include <chrono>
#include <omp.h>
#include <spdlog/spdlog.h>
#include "double_buffer.h"


#ifndef PAGE_SIZE
#define PAGE_SIZE getpagesize()
#endif

#define DMA_BUFFER_COUNT 16
#define ADC_SIMULT_CHANNELS 8
#define ADC_BYTES_PER_CHANNEL_ALIGNED 2.0
#define ADC_BYTES_PER_CLOCK (ADC_SIMULT_CHANNELS*ADC_BYTES_PER_CHANNEL_ALIGNED)
#define DRAGON_BUFFER_ORDER 10U
#define DRAGON_BUFFER_SIZE_BYTES ((1U<<(DRAGON_BUFFER_ORDER))*PAGE_SIZE)
#define FRAME_LENGTH 32768
#define FRAME_COUNT 256

#define N_ITERATIONS 10

using namespace std;
using namespace std::chrono;
using namespace spdlog;

double_buffer<uint16_t> dbuf(ADC_SIMULT_CHANNELS*FRAME_LENGTH*FRAME_COUNT);


int main()
{
    uint8_t* all_buffers[DMA_BUFFER_COUNT];
    for (uint i = 0; i < DMA_BUFFER_COUNT; i ++)
       all_buffers[i] = (uint8_t *)malloc(DRAGON_BUFFER_SIZE_BYTES);
    uint buf_n = 0;
    vector<uint16_t> *dest = dbuf.get_buffer_to_write();
    uint16_t *current_data;

    uint frame_length = FRAME_LENGTH;
    uint frame_count = FRAME_COUNT;
    uint frame_length_bytes = frame_length * (uint)ADC_BYTES_PER_CLOCK;
    uint frames_per_buffer = DRAGON_BUFFER_SIZE_BYTES / frame_length_bytes;
    uint FrameCounter;
    uint16_t* dst = dest->data();

    for (uint rep = 0; rep < 3; rep ++) {
        auto t1 = high_resolution_clock::now();

        FrameCounter = 0;
        while (FrameCounter < frame_count) {
            current_data = (uint16_t *) (all_buffers[buf_n]);
            buf_n = (buf_n + 1) % DMA_BUFFER_COUNT;
            uint16_t *src = current_data;

            #pragma omp parallel for
            for (uint i = 0; i < frame_length; i++)
                for (uint frame = 0; frame < frames_per_buffer; frame++)
                    for (uint a = 0; a < ADC_SIMULT_CHANNELS; a++)
                        dst[frame + FrameCounter + i * frame_count + frame_length * frame_count * a] =
                                src[a + i * ADC_SIMULT_CHANNELS + frame * frame_length * ADC_SIMULT_CHANNELS];

            FrameCounter += frames_per_buffer;
        }

        auto t2 = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(t2 - t1).count();
        info("transpose: {} ms", duration);


        FrameCounter = 0;
        t1 = high_resolution_clock::now();
        while (FrameCounter < frame_count) {
            current_data = (uint16_t *) (all_buffers[buf_n]);
            buf_n = (buf_n + 1) % DMA_BUFFER_COUNT;
            uint16_t *src = current_data;

            #pragma omp parallel for
            for (uint i = 0; i < frame_length; i++)
                for (uint frame = 0; frame < frames_per_buffer; frame++)
                    for (uint a = 0; a < ADC_SIMULT_CHANNELS; a++)
                        dst[a + i * ADC_SIMULT_CHANNELS + frame * frame_length * ADC_SIMULT_CHANNELS] =
                                src[a + i * ADC_SIMULT_CHANNELS + frame * frame_length * ADC_SIMULT_CHANNELS];



            FrameCounter += frames_per_buffer;
        }
        t2 = high_resolution_clock::now();
        duration = duration_cast<milliseconds>(t2 - t1).count();
        info("direct: {} ms", duration);


        FrameCounter = 0;
        t1 = high_resolution_clock::now();
        while (FrameCounter < frame_count) {
            current_data = (uint16_t *) (all_buffers[buf_n]);
            buf_n = (buf_n + 1) % DMA_BUFFER_COUNT;
            uint16_t *src = current_data;

            memcpy(dst, src, ADC_SIMULT_CHANNELS * frame_length * frames_per_buffer * sizeof(uint16_t));

            FrameCounter += frames_per_buffer;
        }
        t2 = high_resolution_clock::now();
        duration = duration_cast<milliseconds>(t2 - t1).count();
        info("memcpy: {} ms", duration);
    }

    return 0;
}
